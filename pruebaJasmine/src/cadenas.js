let contarVocales = {
    "conteoVocales": (palabra) => {
        let vocales = 0;

        for (let i = 0; i < palabra.length; i++) {
            if (palabra.charAt(i) == 'a' || palabra.charAt(i) == 'e' || palabra.charAt(i) == 'i' || palabra.charAt(i) == 'o' || palabra.charAt(i) == 'u') {
                vocales++;
            }

        }
        return vocales;

    },

    "soloLetras": (palabra) => {
        if(palabra.match("[a-zA-Z]")){
            return palabra;
       }
    },
    "stringInvertido": (cadena) => {
        return cadena.split("").reverse().join("");
    }
}

module.exports = contarVocales;

// var cadena = "aAeEiIoOuU son vocales";
// var numeroVocales = cadena.match(/[aeiou]/gi).length;

// console.log(numeroVocales);