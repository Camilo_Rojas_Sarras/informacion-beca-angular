let cadenas = require('../src/cadenas');

describe("pruebas de cadenas con arreglos tipo string:", () => {
    describe("prueba de solo letras", () => {
        it("Cadenas| contar vocales cadena | retorna numero de vocales", () => {
            let palabra = "rene";
            expect(cadenas.conteoVocales(palabra)).toBe(2);
        });

        it("Cadenas| solo letras | retorna palabra de solo letras", () => { //no está bueno
            let palabra = "rene";
            expect(cadenas.soloLetras(palabra)).toBe("rene");
        });
        it("Cadenas| cadena invertida | retorna una palabra invertida", () => {
            let cadena = "rene";
            expect(cadenas.stringInvertido(cadena)).toBe("ener");
        });
    });
});